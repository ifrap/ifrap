# iFRAP

Interactive simulation of the [*Fluorescence Recovery After Photobleaching*](https://en.wikipedia.org/wiki/Fluorescence_recovery_after_photobleaching) process.

This simulates the diffusion of particles as each individual yellow point follows [Brownian motion](https://en.wikipedia.org/wiki/Brownian_motion) within a square box.

![icon.png](icon.png)

Clicking inside the window will change the color of particles around the mouse click. 

The window can be resized.  

This built in C using [OpenGL](https://www.opengl.org) for rendering and [GLFW](https://www.glfw.org) for windowing. The program can be built with [cmake](https://cmake.org) or [make](https://www.gnu.org/software/make/).

## Dependency

You will need a [C++ compiler](https://gcc.gnu.org), [make](https://www.gnu.org/software/make/) and [cmake](https://cmake.org).
In addition, Windowing/Mouse/Keyboard will use:

- [GLFW](https://www.glfw.org)
- [glad](https://glad.dav1d.de)

You first need to download and compile GLFW. The glad headers may be included within the GLFW distribution. Install by copying the files to subdirectory `deps`:

	- deps/libglfw3.a
	- deps/GLFW/glfw3.h
	- deps/glad/gl.h

You will find all these files inside the GLFW project, after you build it.

## Build

```
mkdir build
cd build
cmake ..
make
```

## Running

Depending on the platform (Linux/Mac OS) you may be able to click directly on 'iFRAP', or you may need to start it from the command line.

Using the command line, it is possible to change parameters, eg.:
	
	iFRAP.app/Contents/MacOS/iFRAP n=1

## Integrate with your tools

This code is used in teaching and was conceived as a basis that can easily be extended.
Key action can easily be programmed.

## Authors and acknowledgment

Francois J Nedelec, 13.11.2021 -- 11.10.2022

Development of this code was in part supported by the [Gatsby Charitable Foundation](https://www.gatsby.org.uk), at the [Sainsbury Laboratory at Cambridge University](https://www.slcu.cam.ac.uk)

## License

This is Open Source
