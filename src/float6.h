/*
 Basic diffusion and FRAP simulation
 Francois Nedelec, Copyright Cambridge University 2022
 */

/// OpenGL buffer name
static GLuint stream_ = 0;

/// packs 4 color components and 2 position coordinates
class float6
{
    static const size_t Q = sizeof(float);

public:
    
    /// data
    float P[6];
    
    /// constructor
    float6() {}

    static void initBuffer(size_t nbv)
    {
        size_t nbb = nbv * 6 * Q;
        glGenBuffers(1, &stream_);
        glBindBuffer(GL_ARRAY_BUFFER, stream_);
        glBufferData(GL_ARRAY_BUFFER, nbb, nullptr, GL_STREAM_DRAW);
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);
    }
    
    static void releaseBuffer()
    {
        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteBuffers(1, &stream_);
    }
    
    static float6* mapBuffer()
    {
        return (float6*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    }
    
    static void unmapBuffer()
    {
        glUnmapBuffer(GL_ARRAY_BUFFER);
        glColorPointer(4, GL_FLOAT, 6*Q, nullptr);
        glVertexPointer(2, GL_FLOAT, 6*Q, (void*)(4*Q));
    }
    
    /// pack color components
    void set_rgba(float R, float G, float B, float A)
    {
        P[0] = R;
        P[1] = G;
        P[2] = B;
        P[3] = A;
    }

    /// pack position
    void set_position(float X, float Y)
    {
        P[4] = X;
        P[5] = Y;
    }

    /// pack color components and position
    void set_rgbaxy(float R, float G, float B, float A, float X, float Y)
    {
        P[0] = R;
        P[1] = G;
        P[2] = B;
        P[3] = A;
        P[4] = X;
        P[5] = Y;
    }

};


/// draw a rectangle
static void drawRectangle(float w, float h, float R, float G, float B, float LW)
{
    glLineWidth(LW);
    float6* ptr = float6::mapBuffer();
    ptr[0].set_rgbaxy(R, G, B, 1.0, -w, -h);
    ptr[1].set_rgbaxy(R, G, B, 1.0,  w, -h);
    ptr[2].set_rgbaxy(R, G, B, 1.0,  w,  h);
    ptr[3].set_rgbaxy(R, G, B, 1.0, -w,  h);
    float6::unmapBuffer();
    glDrawArrays(GL_LINE_LOOP, 0, 4);
}
