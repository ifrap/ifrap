/*
 Basic diffusion and FRAP simulation
 Francois Nedelec, Copyright 2022 Cambridge University
 */

#include <cmath>
#include <sstream>
#include <fstream>

int numObjs = 10000;     // number of particles

// physical parameters:
double xBound = 5;      // half-width of box
double yBound = 5;      // half-height of box
double pixelSize = 1;   // size of pixel in display

double diff  = 1;       // diffusion constant
double delta = 0.001;   // time step
double range = 1;       // radius of bleached zone

double delay = 0.032;   // seconds between successive draw
unsigned long seed = 1; // seed for random number generator

// derived quantities below will be calculated from the above values

double alpha   = 0; // diffusive motion within interval of time 'delta'
double simTime = 0; // time in the simulated world


//-----------------------------------------------------------------------------

template <typename T>
int readParameter(const char arg[], const char name[], T & ptr)
{
    bool res = 0;
    char * dup = strdup(arg);
    char * val = dup;
    char * key = strsep(&val, "=");
    while ( isspace(*key) ) ++key;
    if ( *key )
    {
        if ( key == strstr(key, name) ) {
            while ( isspace(*val) ) ++val;
            //printf("Found `%s' in [%s] : %s\n", name, key, val);
            std::istringstream iss(val);
            iss >> ptr;
            res = 1;
        }
    }
    free(dup);
    return res;
}


int readOption(const char arg[])
{
    if ( readParameter(arg, "range", range) )  return 1;
    if ( readParameter(arg, "n",     numObjs) ) return 1;
    if ( readParameter(arg, "count", numObjs) ) return 1;
    if ( readParameter(arg, "diff",  diff) ) return 1;
    if ( readParameter(arg, "diffusion", diff) ) return 1;
    if ( readParameter(arg, "delta", delta) )  return 1;
    if ( readParameter(arg, "seed",  seed) )   return 1;
    if ( readParameter(arg, "delay", delay) )  return 1;
    return 0;
}


void readFile(const char path[])
{
    std::string line;
    std::ifstream is(path);
    if ( !is.good() )
        printf("File `%s' cannot be read\n", path);
    while ( is.good() )
    {
        getline(is, line);
        readOption(line.c_str());
    }
}

