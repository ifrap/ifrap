# Simulation of FRAP

[Fluorescence Recovery After Photobleaching](https://en.wikipedia.org/wiki/Fluorescence_recovery_after_photobleaching)

You will simulate multiple diffusing particles that can be bleached with the mouse. Upon a mouse click, particles within a certain radius will change color. This technique is used to measure diffusion in vivo.

# Physics of diffusing particles

[diffusion](https://en.wikipedia.org/wiki/Diffusion)

### Mobility and drag coefficient

The mobility of a particle of radius `R` is set by [Stokes' law](https://en.wikipedia.org/wiki/Stokes%27_law):

	drag_coefficient = 6 * PI * fluid_viscosity * R;
	mobility_coefficient = 1 / drag_coefficient;

Where `viscosity` is the [viscosity of the fluid](https://en.wikipedia.org/wiki/Viscosity):

	viscosity_water = 0.001 Pa.s = 0.001 pN.s/um^2
	viscosity_cytoplasm = 1 Pa.s = 1 pN.s/um^2

### Directed motion

Under an external force, a particule will move in the direction of applied force:

	speed = mobility_coefficient * force

### Diffusion

The diffusion coefficient is derived from [Einstein's relation](https://en.wikipedia.org/wiki/Einstein_relation_(kinetic_theory)):

	diffusion_coefficient = kT * mobility_coefficient;

Where 'kT' is the amount of thermal energy.
kT is the product of the [Boltzman constant](https://en.wikipedia.org/wiki/Boltzmann_constant) by the [absolute temperature](https://en.wikipedia.org/wiki/Thermodynamic_temperature).
The standard value is ~4.2 nanometer.picoNewton.

The diffusion law specifies that, for an interval of time `tau`, the variable:

	dx = position(t+tau).x - position(t).x

should be [Normally distributed](https://en.wikipedia.org/wiki/Normal_distribution):

	mean(dx) = 0
	variance(dx) = 2 * diffusion_coefficient * tau

This applied also for the y and z components of the position vector.
In the simulation, we may replace the normally-distributed random numbers by something easier to compute.

# Graphical setup

For this project, it is convenient to work with 'pixels' as a physical units, instead of micrometers.

# Particle class

The class 'Object' will represent one particle:
	
	class Particle
	{
	    float x;
	    float y;
	    int color;
	   
	    Particle()
	    {
	        x = 0;
	        y = 0;
	        col = 1;
	    }
	
	    void step()
	    {
				//apply diffusive motion
	    }
	    
	    void bound()
	    {
	        //apply boundary conditions
	    }
	};

# Bleaching

In the main.cc the function 'bleach' will change the color of particles.

	/* bleach particles around ( x, y ) */
	void bleach(double x, double y, double rad)
	{
	}


# Many particles

To simulate multiples particles, they are stored in an array:

	// hard-coded limit to the number of particles
	const size_t MAX = 16384;
	
	Object obj[MAX];
	
There is a hard limit on the max number of particle, but the actual number of particles can be set within this limit.

# Keyboard-Mouse control

The user controls the simulation with mouse and keyboard.
Please spend some time checking the associated callback functions:

	void mouseCallback(...)

	void mouseButtonCallback(...)

	void keyCallback(...)


# Exercices

- Change the diffusion of the particles as a function of position: define a cold area `(X<0)` and a warm area `(X>0)`: observe the result. Can you explain it?

- Include a new state for the Particle: immobilized and diffusing, with stochastic transitions between them.

- Give a radius variable for each particle, from which the diffusion coefficient is calculated using Stokes law. Create multiple populations differing in size.

- Add drift (convection) to the particle.


# Credits

FJN 20.06.2023